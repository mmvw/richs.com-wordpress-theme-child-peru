<?php 
get_header(); 

global $post;
$post = get_post(get_option('page_for_posts'));
setup_postdata($post);
render('page-content');
wp_reset_postdata();
?>

<section class="container posts-wrapper">
    <section class="content">
        <div class="row">
        <?php 
            if(have_posts()):
            loop_posts(function() {
                render('/pages/blog/blog-post');
            });
            else : 
        ?>
            <h2 class='text-richsred text-center'>No posts were found.</h2>
        <?php
            endif;
        ?>
    </div>
        <div class="articles-nav">

            <?php the_posts_pagination( array(
                'mid_size' => 2,
                'prev_text' => __( '<span class="btn btn-red icon-btn reverse"><i class="fas fa-chevron-left blog-fa-left"></i> Newer Posts</span>', 'textdomain' ),
                'next_text' => __( '<span class="btn btn-red icon-btn">Older Posts <i class="fas fa-chevron-right blog-fa-right"></i></span>', 'textdomain' ),
            ) ); ?>
        </div>
    </section>
    <section class="sidebar"> 
        <div class="link-list">
            <h4 class="widget-title">Explore By Category</h4>
            <ul>
            <?php wp_list_categories( array(
                'orderby'    => 'name',
                'title_li' => '',
                'show_count' => false,
                'exclude'    => array(  )
            ) ); ?> 
            </ul>
        </div>

        <?php
        $args = array(
            'numberposts' => 10,
            'orderby' => 'post_date',
            'order' => 'DESC',
            'post_type' => 'post',
            'post_status' => 'publish',
        );
        $recent_posts = wp_get_recent_posts( $args );

        ?>
        <div class="list reverse">
            <h4 class="widget-title">Most Recent Stories</h4>
            <ul class="recent-stories link-list">
                <?php foreach($recent_posts as $recent_post): ?>
                    <li>
                        <a href="<?= get_the_permalink($recent_post['ID']) ?>"><?= $recent_post['post_title'];?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
</section>
<?php
get_footer();
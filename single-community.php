<?php 

get_header(); 

loop_posts(function() {
    render('pages/community/single');
});

get_footer();
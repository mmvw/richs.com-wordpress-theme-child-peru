<?php use Cn\Blocks\Video; ?>

<section class="block hero">
    <div class="background" style="background-image:url(<?= THEME_URL . '/assets/dist/images/richs-gradient-bg.jpg'; ?>); max-height: 407px;">
        <div class="container">
            <div class="flex">
                <div class="content">
                    <h2 class="top-title back-to-leadership"><a href="/our-story/bettering-our-communities/"><i class="fas fa-chevron-left"></i> Back to Bettering our Communities</a></h2>
                    <h1 class="title"><?= the_field('charity_name'); ?></h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block community-section section-1">
    <div class="container">
        <div class="flex">
            <div class="left-container">
                <h2 class="block-title"><?= the_field('charity_title'); ?></h2>
                <div class="post-date-text"><?php echo get_the_date( 'F j, Y' ); ?></div>
                <div class="text">
                    <?= the_field('bio'); ?>
                </div>
            </div>
            <div class="right-container">        
                <div class="button-container">
                    <a href="#" class="btn btn-red btn-print">Print Article <i class="fas fa-print ml-1"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="divider">
    <div class="container">
        <hr class="bg-richsred h-1">
    </div>
</section>
<section class="block community-section section-2">
    <div class="container row">
        <div class="col-md-4">
            <?php $main_image = get_field('main_image'); ?>
            <img src="<?= $main_image['url']; ?>" alt="<?= $main_image['alt']; ?>" />
        </div>
        <div class="col-md-4">
            <?php $supporting_image1 = get_field('supporting_image1'); ?>
            <img src="<?= $supporting_image1['url']; ?>" alt="<?= $supporting_image1['alt']; ?>" />
        </div>
        <div class="col-md-4">
            <?php $supporting_image2 = get_field('supporting_image2'); ?>
            <img src="<?= $supporting_image2['url']; ?>" alt="<?= $supporting_image2['alt']; ?>" />
        </div>
    </div>
    <div class="container row">
        <div class="col-md-4">
            <?php $supporting_image3 = get_field('supporting_image3'); ?>
            <img src="<?= $supporting_image3['url']; ?>" alt="<?= $supporting_image3['alt']; ?>" />
        </div>
        <div class="col-md-4">
            <?php $supporting_image4 = get_field('supporting_image4'); ?>
            <img src="<?= $supporting_image4['url']; ?>" alt="<?= $supporting_image4['alt']; ?>" />
        </div>
        <div class="col-md-4">
            <?php $supporting_image5 = get_field('supporting_image5'); ?>
            <img src="<?= $supporting_image5['url']; ?>" alt="<?= $supporting_image5['alt']; ?>" />
        </div>
    </div>
</section>






<?php if (get_field('display_featured_media') === true) : ?>
    <?php $type = get_field('media_type'); ?>
            <?php if ($type == 'image') : ?>
                <section class="block featured-media">
                    <div class="container">
                        <img src="<?= get_field('featured_media_image')['url']; ?>" />
                    </div>
                </section>
            <?php elseif($type == 'video') :
                $videoParams = get_field('featured_media_video');
                $data = [
                    'name' => 'video',
                    'youtube_id' => $videoParams['youtube_video_id'],
                    'placeholder_image' => $videoParams['placeholder_image']
                ];
                $video = new Video();
                $video->render_from_code($data);
    ?><?php endif; ?>
<?php endif ?>











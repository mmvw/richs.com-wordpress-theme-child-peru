<?php if ($block) : ?>

    <div class="icehot-bar" style="background-color:<?php the_field('background_color'); ?>">
        <div class="row">
            <div class="col-md-4"></div>

                <div class="col-md-4">
                    <?php if($block['image']): ?>
                        <div class="icehot-image-bar">
                            <img src="<?=$block['image']['url']?>">
                        </div>
                    <?php endif; ?>
                </div>

            <div class="col-md-4"></div>
        </div>
    </div>

<?php endif; ?>





   


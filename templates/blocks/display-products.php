<?php 

// vars
$image = get_field('Digital_Features_and_Benefits_3');

?>
<section>
    <div class="container">
        <div class="content-container row">
            <h2 class="block-title"><?= $block['title']; ?></h2>
        </div>
        <div class="product-row row">
            <div> <?php

                $taxonomy = 'product_categories';
                $terms = get_terms($taxonomy); // Get all terms of a taxonomy

                if ( $terms && !is_wp_error( $terms ) ) {
                ?>
                    <div class="product-categories">
                        <?php foreach ( $terms as $term ) {
                            if ($term->parent == 0) { 
                                ?> 
                                <div class="product-category-item">
                                    <a href="<?php echo get_term_link($term->slug, $taxonomy); ?>">
                                        <img src="<?php the_field('category_image', $term); ?>" />
                                        <div class="overlay"></div>
                                        <div class="content-details fadeIn-bottom">
                                            <h3> <?php echo $term->name; ?> </h3>
                                            <p> <?php echo $term->description; ?> </p>
                                        </div>
                                    </a>
                                </div>
                            <?php } else { 
                                // Do nothing
                            }
                    } ?>
                    </div>
                <?php } else { 
                        $args = array(
                        'post_type'   => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => -1 
                        );
                        
                        $products = new WP_Query( $args );
                        if( $products->have_posts() ) :
                        ?>
                
                        <div class="product-list">
                            <?php
                            while( $products->have_posts() ) :
                                $products->the_post();
                                $title = get_the_title();
                                if ($title !== "") {
                                ?>
                                <div class="product-list-item col-ie-md-4"> 
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="img-box">
                                        <?php 
                                         // $image = get_field( "Digital_Features_and_Benefits_1", get_the_ID());
                                        if (has_post_thumbnail( ) ) {
                                            the_post_thumbnail(); 
                                        } else { ?>
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                                            <?php
                                        }
                                         ?>
                                        </div>
                                    <h3><?php echo get_the_title(); ?></h3>
           
                                    </a>
                                </div>
                                <?php
                                }
                            endwhile;
                            wp_reset_postdata();
                            ?>
                        </div>
                
                        <?php
                        else :
                        esc_html_e( 'No products in the taxonomy!' );
                        endif;
                    } ?>
            </div>
        </div>
    </div>
</section>




                    
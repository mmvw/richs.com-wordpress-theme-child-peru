<div class="container">
    <div class="flex">
        <?php foreach ($block['items'] as $item) : ?>
            <div class="item">
                <div class="header">
                    <div class="value"><?= $item['title']; ?></div>
                </div>
                <div class="content">
                    <?php if ($label = $item['label']) : ?>
                        <div class="label"><?= $label; ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php if ($button = $block['button']) : ?>
        <div class="button-container stats-btn">
            <p class="txt-center"><a class="btn btn-white-line" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a></p>
        </div>
    <?php endif; ?>
</div>
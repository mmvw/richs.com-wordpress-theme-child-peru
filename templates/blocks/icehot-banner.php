<?php if ($block) :
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => false,
    ];

    $slide_count = count($block['slides']);
    $show_timer = $slide_count > 1;
    ?>
    <div class="banner-carousel-container">
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php for ($i = 0; $i < count($block['slides']); $i++) : $slide = $block['slides'][$i]; ?>
                <div class="background-wrapper" style="background-image:url(<?= $slide['background_image']['url']; ?>);">
                    <?php
                    $overlay_opacity = 0;
                    if ($slide['add_background_overlay'] === true) {
                        $overlay_opacity = $slide['background_overlay_opacity'];
                    }
                    ?>
                    <div class="overlay gradient <?= $slide['increase_text_contrast'] ? "increased-contrast" : "" ; ?>" style="opacity: <?= $overlay_opacity; ?>;"></div>
                    <div class="container">
                        <div class="content side-<?= $slide['text_side'] ?>">
                            <h2 class="title"><?= $slide['title']; ?></h2>
                                <p class="text"><?= $slide['text']; ?></p>






                                 <?php 

                                // check for rows (parent repeater)
                                if( have_rows('slides') ): ?>
                                    <div class="icon-text">
                                    <?php 

                                    // loop through rows (parent repeater)
                                    while( have_rows('slides') ): the_row(); ?>
                                        <div>
                                            <?php 

                                            // check for rows (sub repeater)
                                            if( have_rows('text_with_icon') ): ?>
                                                <div class="row">
                                                <?php 

                                                // loop through rows (sub repeater)
                                                while( have_rows('text_with_icon') ): the_row();

                                                    // display each item as a list - with a class of completed ( if completed )
                                                    ?>
                                                    <div class="col-md-6">

                                                         <ul>
                                                           <li> 
                                                                <?php $image = get_sub_field('icon_image'); ?>
                                                                <img src="<?php echo $image['url'];?>" />
                                                            </li>
                                                            <li><?php the_sub_field('title_text'); ?></li>
                                                        </ul>
                                                
                                                    </div>
                                                <?php endwhile; ?>
                                                </div> <!--end of ul orgional-->
                                            <?php endif; //if( get_sub_field('items') ): ?>
                                        </div>  

                                    <?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
                                    </div>
                                <?php endif; // if( get_field('to-do_lists') ): ?>





                                <?php if ($button = $slide['button']) : ?>
                                    <a class="btn btn-red icon-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                                <?php endif; ?>


                                <?php if ($show_timer) : ?>
                                    <div class="progress-timer"></div>
                                <?php endif; ?>
                        </div>
                    </div>
        
                </div>
            <?php endfor; ?>
        </slick-carousel>
    </div>
<?php endif; ?>
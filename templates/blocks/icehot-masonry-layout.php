<?php if($block): ?>
    <div class="container">
        
            <div class="text-container">
                <?php if (!empty($block['top_title'])) : ?>
                    <p class="top-title"><?= $block['top_title']; ?></p>
                <?php endif; ?>

                <?php if (!empty($block['title'])) : ?>
                    <h2 class="block-title"><?= $block['title']; ?></h2>
                <?php endif; ?>
                <div class="text"><?= $block['text']; ?></div>
            </div>

           <div class="container">
                <div class="wrapper">
                    <div class="masonry">

                        <?php
                        // check if the repeater field has rows of data
                        if( have_rows('masonry_images') ):
                            // loop through the rows of data
                            while ( have_rows('masonry_images') ) : the_row();
                                // display a sub field value
                                the_sub_field('sub_field_name'); ?>
                                   <div class="brick">

                                        <?php $image = get_sub_field('image'); ?>
                                        <?php $image_link = get_sub_field('image_link'); ?>

                                        <a href="<?= $image_link['url']; ?>" target="<?= $image_link['target']; ?>">
                                            <img src="<?php echo $image['url'];?>" />
                                        </a>
                                     
                                    </div>
                            <?php
                                endwhile;
                            else :
                                // no rows found
                            endif;
                        ?>

                        </div>
                    </div>
                    <?php if ($button = $block['button']) : ?>
                    <p><a class="btn btn-red icehot-red-btn" href="<?= $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a></p>
                    <?php endif; ?>
                </div>
    
    </div>
<?php endif; ?>
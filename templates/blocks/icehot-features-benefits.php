<?php if ($block) : ?>

    <div class="feat-and-benfit" style="background-color:<?php the_field('background_color'); ?>">

        <div class="container">
            <div class="text-container">
                <?php if (!empty($block['top_title'])) : ?>
                    <p class="top-title txt-center"><?= $block['top_title']; ?></p>
                <?php endif; ?>

                <?php if (!empty($block['title'])) : ?>
                    <h2 class="block-title main-blue txt-center"><?= $block['title']; ?></h2>
                <?php endif; ?>
                <div class="copy txt-center"><?= $block['copy']; ?></div>
            </div>
        </div>


        <div class="container-large">
            <div class="wrapper row table">
                 
                <?php 
                // check for rows (parent repeater)
                if( have_rows('feature_benefit') ): ?>
                    <?php 

                    // loop through rows (parent repeater)
                    while( have_rows('feature_benefit') ): the_row(); ?>
                            
                        <!-- vars -->
                        <?php $image = get_sub_field('image'); ?>
                        <?php $feature_title = get_sub_field('feature_title'); ?>
                        <?php $copy = get_sub_field('copy'); ?>

                        <div class="col-md-6">
                            <div class="feat-repeat-blck row">
                                    <div class="col-xs-2">
                                        <img class="product-image" src="<?php echo $image['url'];?>" />
                                    </div>
                                    <div class="col-xs-10 ">
                                        <h4 class="product-title"><?php echo $feature_title; ?></h4>
                                        <?php echo $copy; ?>
                                    </div>
                             </div>   <!-- end of backgroundcolor -->
                        </div> <!-- end of col-6 -->

                       

                    <?php endwhile; // while( has_sub_field('feature_benefit') ): ?>
                <?php endif; // if( get_field('feature_benefit') ): ?>


            </div> <!-- end of wrapper -->
        </div>  <!-- end of container large -->



        <div class="row">
            <div class="col-md-4"></div>

                <div class="col-md-4">
                    <?php if($block['image']): ?>
                        <div class="icehot-image-bar">
                            <img src="<?=$block['image']['url']?>">
                        </div>
                    <?php endif; ?>
                </div>

            <div class="col-md-4"></div>
        </div>
    </div>

<?php endif; ?>


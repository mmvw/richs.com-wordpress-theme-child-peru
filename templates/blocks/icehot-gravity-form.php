<div class="container">
    <div class="form-container <?= $block['style']; ?>">
        <div class="col col-1">
            <h2 class="block-title"><?= $block['title']; ?></h2>
            <p><?= $block['text']; ?></p>
        </div>
        <div class="col col-2">

            <div class="form-container" <?= $block['redirect_after_submission'] ? 'data-redirect="true" data-redirect-url="' . $block['redirect_url'] . '"' : "" ?>>
                <?php gravity_form( $block['gravity_form_id'] , false, false, false, '', true ); ?>
            </div>

        </div>
    </div>
</div>
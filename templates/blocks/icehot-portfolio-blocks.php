<?php if($block): ?>
    <?php
        if ($background = $block['background_image']) {
            $background_image = $background['url'];
        }
    ?>

    <div class="background background-image" style="background-image:url(<?= $background_image; ?>);">


        <div class="container">
            <div class="text-container">
                <?php if (!empty($block['top_title'])) : ?>
                    <p class="top-title txt-center"><?= $block['top_title']; ?></p>
                <?php endif; ?>

                <?php if (!empty($block['title'])) : ?>
                    <h2 class="block-title main-blue txt-center"><?= $block['title']; ?></h2>
                <?php endif; ?>
                <div class="copy txt-center"><?= $block['copy']; ?></div>
            </div>
        </div>

        <div class="container-large">
            <div class="wrapper row">
                 

                <?php 
                // check for rows (parent repeater)
                if( have_rows('product_range_blocks') ): ?>
                    <?php 

                    // loop through rows (parent repeater)
                    while( have_rows('product_range_blocks') ): the_row(); ?>
                            
                        <!-- vars -->
                        <?php $image = get_sub_field('image'); ?>
                        <?php $image_link = get_sub_field('image_link'); ?>
                        <?php $product_title = get_sub_field('product_title'); ?>
                        <?php $product_copy = get_sub_field('product_copy'); ?>

                        <div class="col-md-6">
                            <div class="product-block row button_color-<?php the_sub_field('button_color'); ?>" style="background-color:<?php the_sub_field('background_color'); ?>">


                                    <div class="col-xs-4 col-md-5">
                                        <img class="product-image" src="<?php echo $image['url'];?>" />
                                    </div>
                                    <div class="col-xs-8 col-md-7">
                                        <h3 class="product-title"><?php echo $product_title; ?></h3>
                                        <?php echo $product_copy; ?>
                                        
            

                                        <?php 
                                        // check for rows (sub repeater)
                                        if( have_rows('featuring_flavours') ): ?>
                                            <div class="row flavours-row">
                                            <?php 
                                                // loop through rows (sub repeater)
                                                while( have_rows('featuring_flavours') ): the_row();
                                                    // display each item as a list - with a class of completed ( if completed )
                                                    ?>
                                                    <div class="col-md-3 flavours">
                                                         <!-- vars -->
                                                        <?php $image_flavour = get_sub_field('image_flavour'); ?>
                                                        <?php $text_flavour = get_sub_field('text_flavour'); ?>

                                                        <img src="<?php echo $image_flavour['url'];?>" />

                                                        <h5 class="flavour-txt"><?php echo $text_flavour; ?></h5>
                                                        
                                                    </div>
                                                <?php endwhile; ?>
                                            </div> <!--end of ul orgional-->
                                        <?php endif; //if( get_sub_field('items') ): ?>



                                        <?php
                                        if( $image_link ): ?>
                                            <div class="button button_color-<?= $block['button_color']; ?>">
                                                <a class="button icehot-red-btn" href="<?= $image_link['url']; ?>" target="<?= $image_link['target']; ?>"><?= $image_link['title']; ?></a>
                                            </div>
                                        <?php endif; ?> 

                                    </div>
                             </div>   <!-- end of backgroundcolor -->
                        </div> <!-- end of col-6 -->

                       

                    <?php endwhile; // while( has_sub_field('product_range_blocks') ): ?>
                <?php endif; // if( get_field('product_range_blocks') ): ?>


            </div> <!-- end of wrapper -->
        </div>  <!-- end of container large -->
    </div> <!-- end of background image -->
<?php endif; ?>








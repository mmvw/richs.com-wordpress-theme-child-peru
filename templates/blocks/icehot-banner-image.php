<?php if ($block) : ?>


    <?php
        if ($background = $block['desktop_image']) {
            $background_url_desktop = $background['url'];
        }
        if ($background = $block['tablet_image']) {
            $background_url_tablet = $background['url'];
        }
        if ($background = $block['mobile_image']) {
            $background_url_mobile = $background['url'];
        }
    ?>

    <div class="icehot-banner-desktop" style="background-image:url(<?= $background_url_desktop; ?>);"></div>
    <div class="icehot-banner-tablet" style="background-image:url(<?= $background_url_tablet; ?>);"></div>
    <div class="icehot-banner-mobile" style="background-image:url(<?= $background_url_mobile; ?>);"></div>

<?php endif; ?>

<?php if($block): ?>
    <?php
        if ($background = $block['background_image']) {
            $background_image = $background['url'];
        }
    ?>

    <div class="background background-image" style="background-image:url(<?= $background_image; ?>);">


        <div class="container">
            <div class="text-container">
                <?php if (!empty($block['top_title'])) : ?>
                    <p class="top-title txt-center"><?= $block['top_title']; ?></p>
                <?php endif; ?>

                <?php if (!empty($block['title'])) : ?>
                    <h2 class="block-title main-blue txt-center"><?= $block['title']; ?></h2>
                <?php endif; ?>
                <div class="copy txt-center"><?= $block['copy']; ?></div>
            </div>
        </div>

        <div class="container-large product-range-container">
            <div class="wrapper row product-range-row-container">
                 

                <?php 
                // check for rows (parent repeater)
                if( have_rows('product_range_blocks') ): ?>
                    <?php 

                    // loop through rows (parent repeater)
                    while( have_rows('product_range_blocks') ): the_row(); ?>
                            
                        <!-- vars -->
                        <?php $image = get_sub_field('image'); ?>
                        <?php $image_link = get_sub_field('image_link'); ?>
                        <?php $product_title = get_sub_field('product_title'); ?>
       

                        <div class="product-range-grid col-md-3 col-centered">
                            <div class="product-block-range row" style="background-color:<?php the_sub_field('background_color'); ?>">
                            		<h4 class="product-title"><?php echo $product_title; ?></h4>
                            		<img class="product-image" src="<?php echo $image['url'];?>" />
                             </div>   <!-- end of backgroundcolor -->
                        </div> <!-- end of col-6 -->

                       

                    <?php endwhile; // while( has_sub_field('product_range_blocks') ): ?>
                <?php endif; // if( get_field('product_range_blocks') ): ?>


            </div> <!-- end of wrapper -->
        </div>  <!-- end of container large -->
    </div> <!-- end of background image -->
<?php endif; ?>








<?php

namespace Cn\Acf\CustomPostTypes;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class Communities extends FieldGroup
{

    protected $hide_on_screen = ['the_content'];

    protected function build()
    {
        $this->setLocation('post_type', '==', 'community')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('community');
        $fields
            ->addText('charity_title')
            ->addText('charity_name')
            ->addWysiwyg('bio')
            ->addImage('main_image')
            ->addImage('supporting_image1')
            ->addImage('supporting_image2')
            ->addImage('supporting_image3')
            ->addImage('supporting_image4')
            ->addImage('supporting_image5')
            ->addImage('supporting_image6')
            ->addTrueFalse('display_featured_media')
            ->addRadio('media_type')
                ->conditional("display_featured_media", "==", '1')
                ->addChoice('video')
                ->addChoice('image')
            ->addGroup('featured_media_video')
                ->conditional("display_featured_media", "==", '1')
                ->conditional("media_type", "==", 'video')
                ->addImage("placeholder_image")
                ->addText("youtube_video_id")
            ->endGroup()
            ->addImage('featured_media_image')
                ->conditional("display_featured_media", "==", '1')
                ->conditional("media_type", "==", 'image');            
        return $fields;
    }
}
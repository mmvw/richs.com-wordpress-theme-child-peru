<?php

namespace Ix;

class Acf
{
    use Singleton;

    /**
     * Field group classes will automatially be registered
     */
    protected $field_groups = [
        Acf\Blocks\WorldMap::class,
        Acf\Blocks\TestimonialCarousel::class,
        Acf\Blocks\TextWithImageCarousel::class,
        Acf\Blocks\SidebyImgText::class,
        Acf\Blocks\Notification::class,
        Acf\Blocks\DisplayRecipes::class,
        Acf\Blocks\DisplayProducts::class,
        Acf\Blocks\ContactDetails::class,
        Acf\Blocks\CommunityGrid::class,
        Acf\Blocks\IcehotBanner::class,
        Acf\Blocks\IcehotBannerImage::class,
        Acf\Blocks\IcehotBar::class,
        Acf\Blocks\IcehotFeaturesBenefits::class,
        Acf\Blocks\IcehotFullWidthImage::class,
        Acf\Blocks\IcehotFullWidthLeftImage::class,
        Acf\Blocks\IcehotFullWidthRightImage::class,
        Acf\Blocks\IcehotGravityForm::class,
        Acf\Blocks\IcehotMasonryLayout::class,
        Acf\Blocks\IcehotPortfolioBlocks::class,
        Acf\Blocks\IcehotPortfolioRange::class,
        Acf\Blocks\IcehotPortfolioSingleBlock::class
    ];

    public function __construct()
    {
        $this->register_field_groups();
        $this->register_filters();
    }

    protected function register_field_groups()
    {
        foreach ($this->field_groups as $field_group) {
            (new $field_group)->register();
        }
    }

    protected function register_filters()
    {
        add_filter('acf/fields/post_object/query/name=global_page', [$this, 'filter_global_pages'], 10, 3);
    }

    public function filter_global_pages($args, $field, $post_id)
    {
        
        $args['meta_query'] = [
            'relation' => "AND",
            [
                'key' => '_wp_page_template',
                'value' => 'page-template-global.php',
                'compare' => "="
            ]
        ];
        return $args;
    }

}
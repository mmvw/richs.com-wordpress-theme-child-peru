<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotBanner extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-banner')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-banner');
        $fields
            ->addRepeater('slides')
                ->addRadio('text_side', ['label' => 'Text Side', 'choices' => ['left', 'right']])
                ->addText('title')
                ->addTextArea('text')
                ->addImage('background_image')
                ->addFields($this->getBackgroundOverlay())
                ->addRepeater('text_with_icon')
                    ->addImage('icon_image')
                    ->addText('title_text')
                ->endRepeater()
                ->addLink('button')
            ->endRepeater();
        return $fields;
    }
}
<?php if ($block) :
    $options = [
        'slidesToShow' => 1,
        'slidesToScroll' => 1,
        'prevArrow' => '',
        'nextArrow' => '<button type="button" class="slick-next"></button>',
        'speed' => 800,
        'dots' => false,
    ];

    $slide_count = count($block['slides']);
    $show_timer = $slide_count > 1;
    ?>
    <div class="announcement-carousel-container">
        <slick-carousel :options="<?= htmlspecialchars(json_encode($options), ENT_QUOTES, 'UTF-8') ?>" class="carousel timed-carousel">
            <?php for ($i = 0; $i < count($block['slides']); $i++) : $slide = $block['slides'][$i]; ?>
                <div class="background-wrapper" style="background-image:url(<?= $slide['background_image']['url']; ?>);">
                    <?php
                    $overlay_opacity = 0;
                    if ($slide['add_background_overlay'] === true) {
                        $overlay_opacity = $slide['background_overlay_opacity'];
                    }
                    ?>
                    <div class="overlay gradient <?= $slide['increase_text_contrast'] ? "increased-contrast" : "" ; ?>" style="opacity: <?= $overlay_opacity; ?>;"></div>
                    <div class="container">
                        <div class="content side-<?= $slide['text_side'] ?>">
                            <h2 class="title"><?= $slide['title']; ?></h2>
                                <p class="text"><?= $slide['text']; ?></p>
                                <?php if ($button = $slide['button']) : ?>
                                    <a <?= !empty($slide['recipe_image']) ? 'data-remodal-target="recipe-'.$i.'"' : "";?> class="btn" href="<?= !empty($slide['recipe_image']) ? "#" : $button['url']; ?>" target="<?= $button['target']; ?>"><?= $button['title']; ?></a>
                                <?php endif; ?>
                                <?php if ($show_timer) : ?>
                                    <div class="progress-timer"></div>
                                <?php endif; ?>
                        </div>
                    </div>
                    <?php if($slide['recipe_image']): ?>
                        <div class="remodal photo" data-remodal-options="hashTracking: false" data-remodal-id="recipe-<?=$i; ?>">
                            <button data-remodal-action="close" class="remodal-close"></button>
                            <img class="mx-auto h-full" src="<?=$slide['recipe_image']['url']?>">
                        </div>
                    <?php endif; ?>
                </div>
            <?php endfor; ?>
        </slick-carousel>
    </div>
<?php endif; ?>
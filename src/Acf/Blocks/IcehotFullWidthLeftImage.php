<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotFullWidthLeftImage extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-full-width-left-image')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-full-width-image');
        $fields
            ->addImage('background_image_left')
            ->addImage('background_image_right')
            ->addImage('image')
            ->addSelect('image_size', ['default_value' => "default", 'choices' =>
            [
                ['default' => "Default"],
                ['small' => "Small"]
            ]])
            ->addText('top_title', ['label' => 'Top Title', 'instructions' => "Small title that appears above the title"])
            ->addText('title')
            ->addTextarea('copy', ['instructions' => "(optional) Text to appear below the title"])
            ->addLink('button');                
        return $fields;
    }
}



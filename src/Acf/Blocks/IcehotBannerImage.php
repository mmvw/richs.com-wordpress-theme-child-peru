<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotBannerImage extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-banner-image')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-banner-image');
        $fields
            ->addImage('desktop_image')
            ->addImage('tablet_image')
            ->addImage('mobile_image');
        return $fields;
    }
}
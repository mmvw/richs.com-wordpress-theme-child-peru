<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Cn\Acf\ReusableFields;

class IcehotGravityForm extends FieldGroup
{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-gravity-form')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-gravity-form');
        $fields
            ->addSelect('style', ['default_value' => "two-columns", 'choices' =>
            [
                ['two-columns' => "Two Columns"],
            ]])
            ->addText('title')
            ->addTextArea('text', ['new_lines' => 'br'])
            ->addNumber('gravity_form_id')
            ->addTrueFalse('redirect_after_submission')
            ->addUrl('redirect_url')
                ->conditional('redirect_after_submission', '==', 1)
            ->addText('marketo_id', ['default_value' => "1044", 'label' => "Marketo Form ID"])
            ->addFields($this->getBackgroundColorSelect());
        return $fields;
    }
}

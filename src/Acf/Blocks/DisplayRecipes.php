<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use Cn\Acf\ReusableFields;
use StoutLogic\AcfBuilder\FieldsBuilder;

class DisplayRecipes extends FieldGroup

{
    use ReusableFields;

    protected function build()
    {
        $this->setLocation('block', '==', 'acf/display-recipes')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('display-recipes');
        $fields->addText('title');
        return $fields;
    }
}
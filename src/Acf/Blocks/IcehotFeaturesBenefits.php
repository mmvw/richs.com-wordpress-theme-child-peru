<?php

namespace Ix\Acf\Blocks;

use Cn\Acf\FieldGroup;
use StoutLogic\AcfBuilder\FieldsBuilder;

class IcehotFeaturesBenefits extends FieldGroup
{
    protected function build()
    {
        $this->setLocation('block', '==', 'acf/icehot-features-benefits')
             ->addFields($this->get_fields());
    }

    private function get_fields() {
        $fields = new FieldsBuilder('icehot-features-benefits');
        $fields
            ->addText('top_title')
            ->addText('title')
            ->addColorPicker('background_color')
            ->addRepeater('feature_benefit')
                ->addImage('image')
                ->addText('feature_title')
                ->addWysiwyg('copy')
            ->endRepeater();
        return $fields;
    }
}

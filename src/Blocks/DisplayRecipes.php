<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   BIGBrave
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

// class TextWithLinks extends Block
class DisplayRecipes extends Block
{
    public function __construct()
    {
        parent::register_block(
            'display-recipes',
            [
                'title'           => 'Display Recipes',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'links']
            ]
        );
    }
}
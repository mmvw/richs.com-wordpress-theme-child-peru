<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

// class TextWithLinks extends Block
class SidebyImgText extends Block
{
    public function __construct()
    {
        parent::register_block(
            'sideby-img-text',
            [
                'title'           => 'Sideby Img Text',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'links']
            ]
        );
    }
}
<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotGravityForm extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-gravity-form',
            [
                'title'           => 'Icehot Gravity Form',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['form', 'gravity']
            ]
        );
    }
}
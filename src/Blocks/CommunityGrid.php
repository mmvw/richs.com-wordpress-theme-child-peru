<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark Digital
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class CommunityGrid extends Block
{
    public function __construct()
    {
        parent::register_block(
            'community-grid',
            [
                'title'           => 'Community Grid',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['community', 'organisation']
            ]
        );
    }
}
<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;


class IcehotPortfolioSingleBlock extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-portfolio-single-block',
            [
                'title'           => 'Icehot Portfolio Single Block',
                'category'        => 'layout',
                'icon'            => 'admin-comments',
                'keywords'        => ['text', 'image', 'portfolio']
            ]
        );
    }
}


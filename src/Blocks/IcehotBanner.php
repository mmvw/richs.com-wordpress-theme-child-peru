<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Cypress North
 * @version  $Revision: 3.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotBanner extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-banner',
            [
                'title'           => 'Icehot Banner',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['banner']
            ]
        );
    }
}
<?php

namespace Ix\Blocks;

/**
 * @package  WordPress Theme
 * @author   Benchmark
 * @version  $Revision: 1.0.0
 */

use Cn\FieldGroup;
use Cn\Blocks\Block;

class IcehotMasonryLayout extends Block
{
    public function __construct()
    {
        parent::register_block(
            'icehot-masonry-layout',
            [
                'title'           => 'Icehot Masonry Layout',
                'category'		  => 'layout',
                'icon'			  => 'admin-comments',
                'keywords'		  => ['text', 'image', 'image masonry']
            ]
        );
    }
}
<?php 

// vars
$countryCode = get_field('Country_Code');
$image1 = get_field('Digital_Features_and_Benefits_1');
$image2 = get_field('Digital_Features_and_Benefits_2');
$image3 = get_field('Digital_Features_and_Benefits_3');
$image4 = get_field('Digital_Features_and_Benefits_4');
$image5 = get_field('Digital_Features_and_Benefits_5');
$description = get_field('RomanceDescription');
$descriptionLocal = get_field('RomanceDescriptionLocal');


$ingredients = get_field('IngredientListingLocal');
$sku = get_field('ProductCode');
//$handlingLocal = get_field('HandlingInstructionsLocal');


// extra field not shown in turkey
$handling = get_field('HandlingInstructionsLocal');
$handlingArr = explode(";", $handling);


$benefits = get_field('BenefitsLocal');
$benefitsArr = explode(";", $benefits);


$servingSize = get_field('ServingSize');
$servingSizeUOM = get_field('ServingSizeUOM');
$shelfLifefromMfg = get_field('ShelfLifefromMfg');
$storage = get_field('Storage');
$shelfLifeRefrigeratedPrepared = get_field('ShelfLifeRefrigeratedPrepared');
$shelfLifeAmbientPrepared = get_field('ShelfLifeAmbientPrepared');
$shelfLifeUOM = get_field('ShelfLifeUOM');
$storageLocal = get_field('StorageLocal');


$allergens = get_field('allergens');
$allergensArr = json_decode($allergens, true);
$allergensContainsArray = array();
$allergensMayContainArray = array();
// extra field in India?
$allergensFinalArray = array();


$claims = get_field('claims');
$claimsArr = json_decode($claims, true);


$packagings = get_field('packagings');
$packagingsArr = json_decode($packagings, true);


$categories = get_the_terms( $post->ID , 'product_categories' );
$tags = get_the_terms( $post->ID, 'post_tag' ); 
$parent = get_term_parents_list( $categories[0]->term_id, 'product_categories', array('link' => false) );
$parentName = explode("/", $parent);
$parentSlug = explode("/", $parent);
$productId = $post->ID;

foreach ($packagingsArr as $key => $value ) { 
    $PalletBlockNumberofcases = $value["PalletBlockNumberofcases"];
    $PalletHeightNumberofcases = $value["PalletHeightNumberofcases"];
    $UnitsPerCase = $value["UnitsPerCase"];
    $NetWeightAmountMetric = $value["NetWeightAmountMetric"];
    $WeightMetricUOM = $value["WeightMetricUOM"];
    $GrossWeightMetricUOM = $value["GrossWeightMetricUOM"];
    $UnitAmountofMeasure = $value["UnitAmountofMeasure"];
    $UnitAmount = $value["UnitAmount"];
}


foreach ( $allergensArr as $key => $value ) { 
    if ($value["AllergenLevelofContainmentCodeFromRecipe"] === "10") {
        array_push($allergensContainsArray, $value["AllergenDescription"]);
    } else if ($value["AllergenLevelofContainmentCodeFromRecipe"] === "20") {
        array_push($allergensMayContainArray, $value["AllergenDescription"]);
    } else {
        // do nothing
}


//foreach ($claimsArr as $key => $value ) { 
    //$ClaimCode = $value["ClaimCode"];
//}


}
// ------------------------------------------ BEGINNING OF PAGE ------------------------------------------------

get_header(); 


?>

<div class="product-breadcrumbs" id="crumb">
    <div class="container">
        <?php if ($countryCode === "IN") { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php

        } else if ($countryCode === "TR") { ?>
            <a href="<?php echo get_home_url(); ?>/urunlerimiz">ÜRÜNLERİMİZ</a>
        <?php

        } else if ($countryCode === "PE") { ?>
            <a href="<?php echo get_home_url(); ?>/productos">Productos</a>
        <?php

        } else if ($countryCode === "JP") { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php
        }

        else { ?>
            <a href="<?php echo get_home_url(); ?>/our-products">Products</a>
        <?php } 

      
        if($parentName !== null) { ?>
           / 
            
            <a href="<?php echo get_home_url() . "/product_categories/" . $categories[0]->slug ?>"><?php echo $parentName[0] ?></a>
            <?php
        } 
        ?>
        
    </div>
</div>



<section class="single-product-section">
<div class="container single-product">
        <div class="single-product-grid ie-row">
            <div class="single-product-left col-ie-md-6">
                <div class="content-container row product-image"> 
                        <div id="slider-wrapper">
                        <div id="image-slider">
                            <ul>
                                <li class="active-img">
                                    <img src="<?php echo $image1; ?>" alt="" />
                                </li>
                                <li>
                                    <img src="<?php echo $image2; ?>" alt="" />
                                </li>
                                <li>
                                    <img src="<?php echo $image3; ?>" alt="" />
                                </li>                                           
                            </ul>
                        </div>
                        <div id="thumbnail">
                            <ul>
                                <li class="active col-ie-xs-4"><img src="<?php echo $image1; ?>" alt="" /></li>
                                <li class="col-ie-xs-4"><img src="<?php echo $image2; ?>" alt="" /></li>
                                <li class="col-ie-xs-4"><img src="<?php echo $image3; ?>" alt="" /></li>                
                            </ul>
                        </div>
                        </div>

                </div>
            </div>
            <div class="single-product-right col-ie-md-6">

                <div class="content-container row">
                    <!-- <div class="col-md-6"><img class="peru-icon" src="<?php //echo $image4; ?>" alt="" /></div> -->
                </div>

                <div class="content-container row">
                    <h2 class="product-title"><?php echo get_the_title($post_object->ID); ?></h2>
                    <div class="sku">
                        <?php if ($countryCode === "IN") { ?>
                            <p>SKU CODE <?php echo $sku; ?></p>
                        <?php

                        } else if ($countryCode === "TR") { ?>
                            <p>STOK KODU <?php echo $sku; ?></p>
                        <?php

                        } else if ($countryCode === "PE") { ?>
                            <b><p>CÓDIGO SKU <?php echo $sku; ?></p></b>
                        <?php

                        } else if ($countryCode === "JP") { ?>
                            <p>SKU CODE <?php echo $sku; ?></p>
                        <?php
                        }
                        else { ?>
                            <p>SKU CODE <?php echo $sku; ?></p>
                        <?php } ?>
                    </div>
                    <p class="product-description"><?php echo $descriptionLocal;  ?> </p>
                </div>


                <div class="content-container row product-details">
                    <div class="left-col">
                        <?php 
                        if ($UnitsPerCase !== NULL) { ?>
                            <div>
                                <ul class="product-blocks">
                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/pack-size.svg" alt='placeholder' /></li>
                                    <li>
                                        <p> 
                                            <span class="red">
                                            <?php if ($countryCode === "IN") { ?>
                                                Units per case
                                            <?php

                                            } else if ($countryCode === "TR") { ?>
                                               Units per case
                                            <?php

                                            } else if ($countryCode === "PE") { ?>
                                               Units per case
                                            <?php

                                            } else if ($countryCode === "JA") { ?>
                                                Units per case
                                            <?php
                                            }
                                            else { ?>
                                                Units per case
                                            <?php } ?>
                                            </span>
                                            <br>
                                            <span><?php echo $UnitsPerCase; ?></span>
                                            <!-- <span><?php echo $UnitsPerCase; ?> x <?php echo $UnitAmount; ?> <?php echo $UnitAmountofMeasure; ?></span> -->
                                        </p>
                                    </li>
                                </ul>

    
                            </div> <?php 
                        }
                        ?>
                           

                         <?php 

                        if ($NetWeightAmountMetric !== NULL  || $GrossWeightMetricUOM !== NULL) { ?>
                            <div>
                                <ul class="product-blocks">
                                    <li> <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/pallet-config.svg" alt='placeholder' /></li>
                                     <li>
                                        <p> 
                                        <span class="red">
                                    <?php if ($countryCode === "IN") { ?>
                                        Weight
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                       Ağırlığı
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                       Weight
                                    <?php

                                    } else if ($countryCode === "JP") { ?>
                                        Weight
                                    <?php
                                    }
                                    else { ?>
                                        Weight
                                    <?php } ?>
                                    </span>
                                    <br>
                                    <span><?php //echo $NetWeightAmountMetric . " " . $GrossWeightMetricUOM; ?>
                                    <?php echo $NetWeightAmountMetric . " " . $WeightMetricUOM; ?></span></p>
                                     </li>
                                </ul>
                            </div> <?php 
                        } ?>
                    </div>

                    <div class="right-col">
                    <?php              

                        if ($shelfLifeRefrigeratedPrepared != "") { ?>
                             <div>
                            
                                <ul class="product-blocks">
                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/refrigeration-shelf-life.svg" alt='placeholder' /></li>
                                    <li><p>
                                    <span class="red">
                                    <?php if ($countryCode === "IN") { ?>
                                        Shelf Life
                                    <?php

                                    } else if ($countryCode === "TR") { ?>
                                        Raf Ömrü 
                                    <?php

                                    } else if ($countryCode === "PE") { ?>
                                        Shelf Life
                                    <?php

                                    } else if ($countryCode === "JP") { ?>
                                        Shelf Life
                                    <?php
                                    }
                                    else { ?>
                                        Shelf Life
                                    <?php } ?>
                                    </span>
                                    <br>

                                    <span> <?php echo $shelfLifeRefrigeratedPrepared . " " . $shelfLifeUOM; ?> Refrigerated, <br>
                                           <?php echo $shelfLifeAmbientPrepared . " " . $shelfLifeUOM; ?> Ambient</span>

                                 </p></li>
                                </ul>
                                

                            </div> <?php 
                        } 

                        if ($shelfLifefromMfg !== NULL  || $shelfLifeUOM !== NULL) { ?>


                            <div>
                                <ul class="product-blocks shelf-life-hide">
                                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/refrigeration-shelf-life.svg" alt='placeholder' /></li>
                                    <li>
                                        <p>
                                            <span class="red">
                                            <?php if ($countryCode === "IN") { ?>
                                                Storage
                                            <?php

                                            } else if ($countryCode === "TR") { ?>
                                                Storage
                                            <?php

                                            } else if ($countryCode === "PE") { ?>
                                               Storage
                                            <?php

                                            } else if ($countryCode === "JA") { ?>
                                                Storage
                                            <?php
                                            }
                    
                                            else { ?>
                                                Storage
                                            <?php } ?>
                                            </span>
                                            <br>

                                            <span><?php echo $storage ?></span>
                                         </p>
                                    </li>
                                </ul>
                            </div>

                            <?php 
                        } ?>
                    </div>
                </div>



                <div class="row claim-tag-details">
                        <?php
                        foreach ($claimsArr as $key => $value ) { 

                             if ($value["ClaimCode"] === "Religious Status Halal Certified") {  ?> 
                                 <div class="section-separator-red" ></div>
                                <p>Halaal</p>
                            <?php


                            } else if ($value["ClaimCode"] === "megan") { ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/claim-vegan.svg" alt='placeholder' /> 
                            <?php
                                
                               
                            } else {
                                // do nothing
                                //array_push($allergensMayContainArray, $value["AllergenDescription"]);
                            }
                        } ?>
                    </div>



                <div class="claim-section">
                    <div class="section-separator-red" ></div>

                    <div class="row claim-details">
                        <?php
                        foreach ($claimsArr as $key => $value ) { 
                             if ($value["ClaimCode"] === "Vegan") {  ?> 
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/claim-vegan.svg" alt='placeholder' /> 
                            <?php
                                
                            } else if ($value["ClaimCode"] === "Gluten Free") { ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/claim-gluten-free.svg" alt='placeholder' /> 
                            <?php
                              
                            } else if ($value["ClaimCode"] === "Dairy Free") { ?>
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/Icons/claim-dairy-free.svg" alt='placeholder' /> 
                            <?php
                               
                            } else {
                                // do nothing
                                //array_push($allergensMayContainArray, $value["AllergenDescription"]);
                            }
                        } ?>

                    </div>
                </div>


            


                 <?php
                if ($benefits !== "" ) { ?>
                            <?php if ($countryCode === "IN") { ?>
                                <h2><?php _e( 'FEATURES AND BENEFITS' ); ?></h2>
                            <?php

                            } else if ($countryCode === "TR") { ?>
                               <h2> <?php _e( 'FEATURES AND BENEFITS' ); ?></h2>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                               <h2> <?php _e( 'FEATURES AND BENEFITS' ); ?></h2>
                            <?php

                            } else if ($countryCode === "JP") { ?>
                               <h2> <?php _e( 'FEATURES AND BENEFITS' ); ?></h2>
                            <?php
                            }
    
                            else { ?>
                               <h2> <?php _e( 'FEATURES AND BENEFITS' );?></h2>
                            <?php } ?>
                        </a>


                        <ul class="p-list"> <?php 
                           foreach ( $benefitsArr as $item ) {  ?>
                               <p> <li> <?php echo $item; ?></li> </p><?php
                            } ?>
                        </ul>
                      <?php 
                    }
                ?>

                 <div class="row"> 

                      <?php 
                   if( !empty( $image5 ) ): ?>
                       
                    
                    <div class="col-md-6">
                        <div class="solid-btn">
                            <?php if ($countryCode === "IN") { ?>
                                <a href="<?php echo $image5; ?>"> Download Factsheet</a>
                            <?php

                            } else if ($countryCode === "TR") { ?>
                                <a href="<?php echo $image5; ?>"> Download Factsheet </a>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                                <a href="<?php echo $image5; ?>"> Download Factsheet </a>
                            <?php

                            } else if ($countryCode === "JP") { ?>
                              <a href="<?php echo $image5; ?>"> Download Factsheet </a>
                            <?php
                            }
                            else { ?>
                              <a href="<?php echo $image5; ?>"> Download Factsheet </a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="col-md-6">
                        <div class="ghost-btn">
                            <?php if ($countryCode === "IN") { ?>
                                <a href="/contactanos/">FIND OUT MORE</a>
                            <?php

                            } else if ($countryCode === "TR") { ?>
                                <a href="/contactanos/">FIND OUT MORE</a>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                                <a href="/contactanos/">MÁS INFORMACIÓN</a>
                            <?php

                            } else if ($countryCode === "JP") { ?>
                               <a href="/contactanos/">FIND OUT MORE</a>
                            <?php
                            }
                            else { ?>
                              <a  href="/contactanos/">FIND OUT MORE</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>          
            </div>
        </div>

        <div class="single-product-tabs">

            <div id="tabs">
                <ul> 
                    <?php
                    if ($handling !== "" ) { ?>
                        <li>
                            <a href="#tabs-1">
                            <?php if ($countryCode === "IN") { ?>
                                 <h2><?php _e( 'TIPS & HANDLING' ); ?></h2> 
                            <?php

                            } else if ($countryCode === "TR") { ?>
                                 <h2><?php _e( 'KULLANIM TALİMATLARI' ); ?></h2>
                            <?php

                            } else if ($countryCode === "PE") { ?>
                                <h2><?php _e( '¿Cómo usarlo?' ); ?></h2>
                            <?php

                            } else if ($countryCode === "JP") { ?>
                                <h2><?php _e( 'TIPS & HANDLING' ); ?></h2>
                            <?php
                            }
                            else { ?>
                                <h2><?php _e( 'TIPS & HANDLING' ); ?></h2>
                            <?php } ?>
                            </a>
                        </li> <?php 
                        }  ?>

                        <li>
                        
                        <a href="#tabs-2">
                            <?php if ($countryCode === "IN") { ?>
                                   <?php _e( 'RECIPES' ); ?>
                                <?php

                                } else if ($countryCode === "TR") { ?>
                                     <?php // _e( 'RECIPES' ); ?>
                                <?php

                                } else if ($countryCode === "PE") { ?>
                                     <?php _e( 'RECIPES' ); ?>
                                <?php

                                } else if ($countryCode === "JA") { ?>
                                     <?php _e( 'RECIPES' ); ?>
                                <?php
                                }
        
                                else { ?>
                                    <?php _e( 'RECIPES' ); ?>
                            <?php } ?>
                        </a>
                        </li>

                </ul> 

                <?php 
                if ($handling !== "" ) { ?>
                    <div id="tabs-1">
                        <ul class="p-list"> <?php 
                           foreach ( $handlingArr as $item ) {  ?>
                               <p> <li> <?php echo $item; ?></li> </p><?php
                            } ?>
                        </ul>
                     </div>  <?php 
                } ?>


                <div id="tabs-2">
                    <div class="product-list">
                    <?php
                       
                    global $post;
                    $args = array( 'numberposts' => -1, 'post_type'=> 'recipes');
                    $posts = get_posts($args);
                    $thereAreRecipes = false;
                  
                    
                    foreach( $posts as $post ) : setup_postdata($post); 
                        // var_dump($post);
                        $postId = $post->post_ID;
                      
                        $featured_img_url = get_the_post_thumbnail_url($postId,'full');
                        $postTitle = $post->post_title;
                        $postName = $post->post_name;
                        $postidchantier = $post->ID;
                        $relatedProducts = get_field('related_products', $postidchantier);
                     
                        if ($relatedProducts) { ?>
                            <!-- <div class="product-list">  --><?php
                            foreach ($relatedProducts as $relatedProduct) {
                                foreach ($relatedProduct as $product) {  
                                    if ($product->ID == $productId) {
                                        $thereAreRecipes = true;
                                        ?>


                                        <div class="product-list-item">

                                            <?php if ( is_user_logged_in() ) { ?>
                                            <a href="<?php echo get_home_url() . "/recipe/" . $postName; ?>">
                                                <div class="image-container">
                                                    <img src="<?php echo $featured_img_url ?>" alt="product" />
                                                </div>
                                                <h3><?php echo $postTitle; ?></h3>
                                            </a>
                                            <?php } else { ?>
                                            <a  href="<?php echo get_home_url() . "/user/"; ?>">
                                                <div class="image-container">
                                                    <img class="category-lock category-product-lock" src="<?php echo get_stylesheet_directory_uri(); ?>/recipe-includes/images/lock-alt-solid.svg" alt='placeholder' />
                                                    <img src="<?php echo $featured_img_url ?>" alt="product" />
                                                </div>
                                                <h3><?php echo $postTitle; ?></h3>
                                            </a>
                                            <?php } ?>

                                        </div>
                                        
                                        <?php
                                    } 
                                }
                            } ?>
                            <!-- </div> -->
                        <?php } 
                endforeach; 
                    ?>
                    </div>
                </div>
 
            </div>
        </div>










        <div class="related-products container">

            <?php if ($countryCode === "IN") { ?>
               <h2>RELATED PRODUCTS</h2>
            <?php

            } else if ($countryCode === "TR") { ?>
               <!--  <h2>RELATED PRODUCTS</h2> -->
            <?php

            } else if ($countryCode === "PE") { ?>
                <h2>RELATED PRODUCTS</h2>
            <?php

            } else if ($countryCode === "JA") { ?>
                <h2>RELATED PRODUCTS</h2>
            <?php
            }

            else { ?>
                <h2>RELATED PRODUCTS</h2>
           <?php } ?>

            
            <div class="product-list">
            <?php 
             $categoryArgs = array(
                'post_type'   => 'product',
                'post_status' => 'publish',
                'posts_per_page' => 3,
                'post__not_in' => array( $post->ID ),
                'tax_query'   => array(
                    array(
                        'taxonomy' => 'product_categories',
                        'field'    => 'slug',
                        'terms'    => $categories[0]->slug
                    )
                )
            );
            $categoryProducts = new WP_Query( $categoryArgs );
            while( $categoryProducts->have_posts() ) :
                $categoryProducts->the_post();
                ?>
                <div class="product-list-item">
                    <a href="<?php echo get_the_permalink(); ?>">
                    <div class="image-container">
                    <?php if (has_post_thumbnail( $post->ID ) ) {
                        the_post_thumbnail(); 
                    } else { ?>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/includes/placeholder.jpg" alt='placeholder' />
                        <?php
                    } ?>
                    </div>
                        <h3 class="product-title"><?php echo get_the_title(); ?></h3>
                        <?php $member_tags = get_the_terms( get_the_ID(), 'post_tag' ); 
                        foreach ( $member_tags as $tag) { 
                            if ($tag->name == "New") {?>
                                <div class="product-tag">
                                    <span><?php echo $tag->name; ?></span>
                                </div>
                                <?php
                            }
                        }
                        ?>     
                    </a>
                </div> 
            <?php
            endwhile;
            ?>
            </div> 
        </div>
</section>
<?php


get_footer();
